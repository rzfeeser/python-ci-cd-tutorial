#!/usr/bin/python3
"""Alta3 Research | RZFeeser@alta3.com
   A simple script to demo CI / CD within GitLab using Python"""

import sys

# python3 -m pip install crayons
import crayons

TANUKI = """
 
                              .                : :
            _..----..__   __..:'.-'''-.-''    .  :
          .'      ,    '''    '    :   .'    /  '
         ',                  ( -=o):(o=-)   .  :
        :     ,               ''.  ;  .'  __:  :
        :          :      ,      '.0.''.-'.))  :  __..--
        :           :                ._.-'__| ':''.
         .           :   ,   ..  :.-' __.' /   ;    . 
        .'       ,   :    _.'  '. '.''    /   /  '
      .:. .'.        :--:'_..--'''.))  .  ' -'    __.--'
    .''::'   '-.  .-''.  '.   .             __.--'
    :...:     __\  '.  '..))     '    __.--'
    ::'':.--''   '.)))          __.--'
_..--:.::'   .         .  __.--'
      :' .:.        __.--'
  '    .::' : __.--'
xxx  __' .::'
..--''   ':::'
 
"""


SNAKE = """
                                     ..:::::;'|
             __   ___             / \:::::;'  ;
           ,::::`'::,`.          :   ___     /
          :_ `,`.::::)|          | ,'SSt`.  /
          |(` :\)):::`;          |:::::::| :
          : \ ,`'`:::::`.        |:::::::| |
           \ \  ,' `:::::`.      :\::::::; |
            \ `.  ,' ` ,--.)     : `----'  |
             :  `-.._,'__.'      :   ____  |
             |     |              :,'::::\ |
             :  _  |              :::::::::|
             ;     :              |:::::::||
            :      |              |:::::::;|
            :  __  :              |\:::::; |
            |      |              | _____  |
            |      :              |':::::\ |
            : .--  |\             |::::::::|
            :      :.\            |:::::::;|
             :      \:\           |::::::/ |
              \ .-'  \'`.         ;`----' /;
               \      \|::-...__,'._     //
                `.  ,' `:::/ |::::::`. ,'/
                  `.     `-._;:::::::.','
                    `-..__,   ``--'' ,'
                          ``---....-'
"""


# ascii art generator
def asciiart(animal, initials):
    """create a finished ASCII art"""
    return f"{animal}\nCreated By: {initials}"




def main():
    """run-time code"""

    initals = "rzf" # artist initals

    art = asciiart(TANUKI, initals)
    if art is None:
        print("Something went wrong while creating the art")
        sys.exit()
    else:
        print(f"{str(crayons.red(art))}")   # create colorful ascii art


# call the main function
if __name__ == "__main__":
    main()
